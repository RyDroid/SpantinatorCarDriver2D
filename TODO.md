# Things to do

## Bugs

* function `astar_search_unsafe` of astar_path.c does not work.
* There are memory problems (valgrind is your friend).

## Additions

* Use the astar result for a driver.

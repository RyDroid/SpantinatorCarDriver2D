# Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


SRC_DIR=src
INC_DIR=src
OBJ_DIR=obj
BIN_DIR=bin
DOC_DIR=doc

CC=gcc
INCLUDES=-I$(INC_DIR) -Isimulator/ry-simulator/src/utils -Isimulator/ry-simulator/src/specific
DEBUG_FLAGS=-D_FORTIFY_SOURCE=0 -O0 -g -DDEBUG
CFLAGS=-std=c99 -Wall -Wextra -Wpedantic \
	-D_FORTIFY_SOURCE=0 -O3 -march=native \
	$(INCLUDES) $(DEBUG_FLAGS)
LIBS=-lm
RM=rm -f

PACKAGE=spantinator-driver
FILES_TO_ARCHIVE=$(SRC_DIR)/ $(INC_DIR)/ simulator/ etc/ makefile presentation/ \
	LICENSE* TODO* \
	.gitignore .editorconfig .dir-locals.el


.PHONY: \
	bin simulator presentation doc documentation \
	archives default-archive zip tar-gz tar-bz2 tar-xz 7z \
	clean clean-bin clean-profiling \
	clean-ide clean-qt-creator clean-codeblocks \
	clean-tmp clean-archives clean-latex \
	clean-git


all: tests doc presentation default-archive


launch: simulator-bin drivers
	cd simulator/ && make launch


bin: tests drivers

drivers: default-driver

default-driver:
	@echo 'TODO create a default driver'

tests: $(BIN_DIR)/test-astar

$(BIN_DIR)/test-astar: $(OBJ_DIR)/test-astar.o
	@mkdir -p $(BIN_DIR)
	$(CC) $(CFLAGS) \
		$(OBJ_DIR)/test-astar.o simulator/ry-simulator/src/utils/tab_2d_char_plus.c \
		-o $(BIN_DIR)/test-astar $(LIBS)

$(OBJ_DIR)/test-astar.o: \
		$(SRC_DIR)/test-astar.c \
		$(INC_DIR)/*.h \
		simulator-source simulator/ry-simulator/src/utils/*.h \
		simulator/ry-simulator/src/specific/*.h
	@mkdir -p $(OBJ_DIR)
	$(CC) $(CFLAGS) -c $(SRC_DIR)/test-astar.c -o $(OBJ_DIR)/test-astar.o


tracks: simulator-source
	cd simulator && make tracks

simulator: simulator-source
	cd simulator && make

simulator-bin: simulator-source
	cd simulator && make bin

simulator-source:
	git submodule init   simulator
	git submodule update simulator


read-doc-html: doc-html
	xdg-open $(DOC_DIR)/html/index.html &

read-doc-pdf: doc-pdf
	xdg-open $(DOC_DIR)/html/index.html &

doc: documentation

documentation: doc-html doc-latex

doc-latex: doc-html
	cd $(DOC_DIR)/latex/ && make
	cd simulator && make doc-latex

doc-html: $(INC_DIR) etc/doxygen_configuration.ini
	doxygen etc/doxygen_configuration.ini
	cd simulator && make doc-html


presentation:
	cd presentation/ && make pdf


archives: zip tar-gz tar-bz2 tar-xz 7z

default-archive: tar-xz

zip: $(PACKAGE).zip

$(PACKAGE).zip: $(FILES_TO_ARCHIVE)
	zip $(PACKAGE).zip -r -- $(FILES_TO_ARCHIVE) > /dev/null

tar-gz: $(PACKAGE).tar.gz

$(PACKAGE).tar.gz: $(FILES_TO_ARCHIVE)
	tar -zcvf $(PACKAGE).tar.gz -- $(FILES_TO_ARCHIVE) > /dev/null

tar-bz2: $(PACKAGE).tar.bz2

$(PACKAGE).tar.bz2: $(FILES_TO_ARCHIVE)
	tar -jcvf $(PACKAGE).tar.bz2 -- $(FILES_TO_ARCHIVE) > /dev/null

tar-xz: $(PACKAGE).tar.xz

$(PACKAGE).tar.xz: $(FILES_TO_ARCHIVE)
	tar -cJvf $(PACKAGE).tar.xz -- $(FILES_TO_ARCHIVE) > /dev/null

7z: $(PACKAGE).7z

$(PACKAGE).7z: $(FILES_TO_ARCHIVE)
	7z a -t7z $(PACKAGE).7z $(FILES_TO_ARCHIVE) > /dev/null


clean: clean-bin clean-profiling clean-ide clean-tmp clean-archives clean-latex
	$(RM) -rf -- \
		doc/ documentation/ \
		*.pyc __pycache__ *.elc \
		.*rc .bash* .zsh* .R* .octave* .xsession*
	cd $(SRC_DIR)   && make --directory=.. clean-bin clean-tmp
	cd simulator    && make clean
	cd presentation && make clean

clean-bin:
	$(RM) -rf -- \
		$(OBJ_DIR) $(BIN_DIR) \
		*.o *.a *.so *.ko *.dll *.out

clean-profiling:
	$(RM) -rf -- callgrind.out.*

clean-ide: clean-qt-creator clean-codeblocks

clean-qt-creator:
	$(RM) -f -- qmake_makefile *.pro.user

clean-codeblocks:
	$(RM) -f -- *.cbp *.CBP

clean-tmp:
	$(RM) -r -- \
		*~ *.bak *.backup .\#* \#* \
		*.sav *.save *.autosav *.autosave \
		*.log *.log.* error_log* \
		.cache/ .thumbnails/ \
		.CACHE/ .THUMBNAILS/

clean-archives:
	$(RM) -rf -- \
		*.deb *.rpm *.exe *.msi *.dmg *.apk *.ipa \
		*.DEB *.RPM *.EXE *.MSI *.DMG *.APK *.IPA \
		*.tar.* *.tgz *.gz *.bz2 *.lz *.lzma *.xz \
		*.TAR.* *.TGZ *.GZ *.BZ2 *.LZ *.LZMA *.XZ \
		*.zip *.7z *.rar *.jar \
		*.ZIP *.7Z *.RAR *.JAR \
		*.iso *.ciso *.img *.gcz *.wbfs \
		*.ISO *.CISO *.IMG *.GCZ *.WBFS

clean-latex:
	$(RM) -f -- \
		*.pdf *.dvi *.ps \
		*.PDF *.DVI *.PS \
		*.acn *.aux *.bcf *.cut *.fls *.glo *.ist *.lof *.nav \
		*.run.xml *.snm *.toc *.vrb *.vrm *.xdy \
		*.fdb_latexmk *-converted-to.* \
		*.synctex *.synctex.gz *.synctex.bz2 *.synctex.xz \
		*.synctex.zip *.synctex.rar

clean-git:
	git clean -fdx

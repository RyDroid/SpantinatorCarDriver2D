/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief Functions related to a 2D graph.
 */


#ifndef GRAPH_2D_H
#define GRAPH_2D_H


#include "map_positions.h"
#include "map_2d_tile.h"


/* 012
 * 3n4
 * 567
 * n is the node
 */
#define NB_NEIGHBORS 8


typedef map_2d_tile graph_2d;
typedef graph_2d graph;
typedef map_position graph_2d_node;
typedef graph_2d_node graph_node;
typedef graph_node node;
typedef map_tile node_value;


/**
 * @brief Returns the value of a node.
 * @param map A not null pointer on a graph/map with nodes
 * @param node A not null pointer on a node
 * @return The value of a node
 */
static inline
node_value
get_node_value_unsafe(const graph* map,
		      const node* a_node)
{
  return map_2d_tile_get_element_value_unsafe(map, a_node->x, a_node->y);
}

/**
 * @brief Checks if a node is possible for a graph.
 * @param map A not null pointer on a graph/map with nodes
 * @param node A not null pointer on a node
 * @return True if a node is possible for a graph, otherwise false
 */
static inline
bool
is_possible_node_struct_unsafe(const graph* map,
			       const node* node)
{
  return map_tile_is_possible_for_a_car(get_node_value_unsafe(map, node));
}

/**
 * @brief Checks if a node position is possible for a graph.
 * @param map A not null pointer on a graph/map with nodes
 * @param node_x X coordinate of the node
 * @param node_y Y coordinate of the node
 * @return True if a node position is possible for a graph, otherwise false
 */
static inline
bool
is_possible_node_values_unsafe(const graph* map,
			       const unsigned int node_x,
			       const unsigned int node_y)
{
  return
    map_tile_is_possible_for_a_car(map_2d_tile_get_element_value_unsafe(map,
									node_x,
									node_y));
}

/**
 * @brief Returns the number of neighbors of a node.
 * @param map A not null pointer on a graph/map with nodes
 * @param a_node A not null pointer on a node with potential neighbors
 * @return The number of neighbors of a node
 */
static inline
unsigned char
get_nb_possible_neighbors_of_node_unsafe(const graph* map,
					 const node* a_node)
{
  unsigned char nb = 0;
  if(is_possible_node_values_unsafe(map, a_node->x -1, a_node->y -1))
    {
      ++nb;
    }
  if(is_possible_node_values_unsafe(map, a_node->x,    a_node->y -1))
    {
      ++nb;
    }
  if(is_possible_node_values_unsafe(map, a_node->x +1, a_node->y -1))
    {
      ++nb;
    }
  if(is_possible_node_values_unsafe(map, a_node->x -1, a_node->y))
    {
      ++nb;
    }
  if(is_possible_node_values_unsafe(map, a_node->x +1, a_node->y))
    {
      ++nb;
    }
  if(is_possible_node_values_unsafe(map, a_node->x -1, a_node->y +1))
    {
      ++nb;
    }
  if(is_possible_node_values_unsafe(map, a_node->x,    a_node->y +1))
    {
      ++nb;
    }
  if(is_possible_node_values_unsafe(map, a_node->x +1, a_node->y +1))
    {
      ++nb;
    }
  return nb;
}


#endif

/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief Functions to compute costs to go to a node to an other node.
 */


#ifndef ASTAR_COSTS_H
#define ASTAR_COSTS_H


#include "astar_cost.h"


DEFINE_TAB_2D_GENERIC_STATIC(astar_cost)


/**
 * @brief Returns the node with the minimum value.
 * @param tab A 2D table with astar costs
 * @return The node with the minimum value
 */
static inline
node
tab_2d_astar_cost_get_node_with_min_value_unsafe(const tab_2d_astar_cost* tab)
{
  astar_cost min_value = tab->tab[0];
  astar_cost current_value;
  node min_node = { 0, 0 };

  for(unsigned int line=0, column; line < tab->nb_lines; ++line)
    {
      for(column=0; column < tab->nb_columns; ++column)
	{
	  current_value =
	    TAB_2D_GENERIC_STATIC_POINTER_GET_ELEMENT_VALUE_UNSAFE(tab,
								   line,
								   column,
								   astar_cost);
	  if(current_value > 0 && current_value < min_value)
	    {
	      min_value = current_value;
	      min_node.x = column;
	      min_node.y = line;
	    }
	}
    }
  return min_node;
}


#endif

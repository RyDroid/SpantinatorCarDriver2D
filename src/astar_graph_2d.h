/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief Structures and functions for a A-star algorithm with a 2D graph.
 */


#ifndef ASTAR_GRAPH_2D_H
#define ASTAR_GRAPH_2D_H


#include "graph_2d.h"


/**
 * @brief A node with his parent/predecessors.
 * It can used by a A-star algorithm.
 */
struct astar_node
{
  graph_node node;
  graph_node parent;
};
typedef struct astar_node astar_node;

DEFINE_TABLE_TINY_OF(astar_node)
/**
 * @brief A 1D table of neighbors of a node.
 */
typedef table_tiny_of_astar_node table_of_astar_neighbors;


static inline
unsigned char
get_nb_astar_neighbors(const graph* map,
		       const node* neighbors)
{
  return
    neighbors == NULL
    ? 0
    : get_nb_astar_neighbors_unsafe(neighbors);
}

/**
 * @brief Returns the neighbors of a node of a graph.
 * @param map Graph/Map with nodes
 * @param node Node with potential neighbors wanted
 * @return The neighbors of a node of a graph
 */
static inline
table_of_astar_neighbors
get_astar_neighbors_of_node_unsafe(const graph* map,
				   const node* node)
{
  table_tiny_of_node neighbors =
    get_possible_neighbors_of_node_unsafe(map, node);
  table_of_astar_neighbors astar_neighbors;
  
  astar_neighbors.size = neighbors.size;
  if(astar_neighbors.size == 0)
    {
      astar_neighbors.elements = NULL;
      return tab;
    }

  astar_neighbors.elements =
    (node*) malloc(astar_neighbors.size * sizeof(node));
  if(astar_neighbors.elements == NULL)
    {
      astar_neighbors.size = 0;
      return astar_neighbors;
    }

  for(unsigned char i=0; i < neighbors.size; ++i)
    {
      astar_neighbors.elements[i].node   = neighbors.elements[i];
      astar_neighbors.elements[i].parent = node;
    }
  
  free(neighbors.elements);
  return astar_neighbors;
}


#endif

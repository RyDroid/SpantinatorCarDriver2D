/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief A structure and functions to multiple nodes of a graph.
 */


#ifndef GRAPH_NODES_H
#define GRAPH_NODES_H


#include "graph_2d.h"
#include "positions_2d_uint.h"


typedef table_of_position_2d_uint table_of_node;


/**
 * @brief Checks if a table of node contains a node.
 * @param tab A not null pointer on a non empty table of nodes
 * @param a_node A node to search in the table
 * @return True if a table of node contains a specified node, otherwise false
 */
static inline
bool
table_of_node_contains_unsafe(const table_of_node* tab,
			      const node* a_node)
{
  for(unsigned int i=0; i < tab->size; ++i)
    {
      if(position_2d_uint_equals(tab->elements[i], *a_node))
	{
	  return true;
	}
    }
  return false;
}

static inline
unsigned char
get_nb_possible_neighbors_unsafe(const graph* map,
				 const table_of_node* neighbors)
{
  unsigned char nb=0, i;
  for(i=0; i < neighbors->size; ++i)
    {
      if(is_possible_node_struct_unsafe(map, &neighbors->elements[i]))
	{
	  ++nb;
	}
    }
  return nb;
}

/**
 * @brief Returns valid neighbors of a graph.
 * @param map A not null pointer on a graph.
 * @param node A not null pointer on a node with potential neighboors
 * @return Valid neighbors of a graph
 */
static inline
table_of_node
get_possible_neighbors_of_node_unsafe(const graph* map,
				      const node* node)
{
  table_of_node tab;
  
  tab.size = get_nb_possible_neighbors_of_node_unsafe(map, node);
  if(tab.size == 0)
    {
      tab.elements = NULL;
      return tab;
    }

  tab.elements = malloc(tab.size * sizeof(node));
  if(tab.elements == NULL)
    {
      tab.size = 0;
      return tab;
    }
  
  unsigned int i = 0;
  if(is_possible_node_values_unsafe(map, node->x -1, node->y -1))
    {
      tab.elements[i].x = node->x -1;
      tab.elements[i].y = node->y -1;
      ++i;
    }
  if(is_possible_node_values_unsafe(map, node->x,    node->y -1))
    {
      tab.elements[i].x = node->x;
      tab.elements[i].y = node->y -1;
      ++i;
    }
  if(is_possible_node_values_unsafe(map, node->x +1, node->y -1))
    {
      tab.elements[i].x = node->x +1;
      tab.elements[i].y = node->y -1;
      ++i;
    }
  if(is_possible_node_values_unsafe(map, node->x -1, node->y))
    {
      tab.elements[i].x = node->x -1;
      tab.elements[i].y = node->y;
      ++i;
    }
  if(is_possible_node_values_unsafe(map, node->x +1, node->y))
    {
      tab.elements[i].x = node->x +1;
      tab.elements[i].y = node->y;
      ++i;
    }
  if(is_possible_node_values_unsafe(map, node->x -1, node->y +1))
    {
      tab.elements[i].x = node->x -1;
      tab.elements[i].y = node->y +1;
      ++i;
    }
  if(is_possible_node_values_unsafe(map, node->x,    node->y +1))
    {
      tab.elements[i].x = node->x;
      tab.elements[i].y = node->y +1;
      ++i;
    }
  if(is_possible_node_values_unsafe(map, node->x +1, node->y +1))
    {
      tab.elements[i].x = node->x +1;
      tab.elements[i].y = node->y +1;
      ++i;
    }
  return tab;
}


#endif

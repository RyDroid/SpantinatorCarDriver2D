/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief Functions to find a path thanks to A-star algorithm.
 */


#ifndef ASTAR_PATH_H
#define ASTAR_PATH_H


#include "astar_costs.h"
#include "graph_nodes.h"
#include <float.h>


DEFINE_TAB_2D_GENERIC_STATIC(node)


/**
 * @brief Returns the path computed thanks to A-star algorithm.
 * IT DOES NOT WORK.
 * @param start_node A not null pointer on a starting node.
 * @param goals A not null pointer on a table of goals.
 */
static inline
tab_2d_node
astar_search_unsafe(const node* start_node,
		    const table_of_goal* goals,
		    const graph* map)
{
  tab_2d_astar_cost tab_2d_costs;
  tab_2d_costs.nb_lines   = map->nb_lines;
  tab_2d_costs.nb_columns = map->nb_columns;
  /* TODO fix segfault and remove doc not-working */
  TAB_2D_GENERIC_STATIC_POINTER_ALLOC_NULL_UNSAFE(&tab_2d_costs, astar_cost);
  TAB_2D_GENERIC_STATIC_POINTER_SET_ELEMENT_VALUE_UNSAFE(&tab_2d_costs, start_node->y, start_node->x, 1000000000, astar_cost);
  
  tab_2d_char is_elements_closed = tab_2d_char_create_with_null_values(map->nb_lines, map->nb_columns);
  /* 0/false = not closed element, 1/true = closed element */
  
  tab_2d_node came_from;
  came_from.nb_lines   = map->nb_lines;
  came_from.nb_columns = map->nb_columns;
  TAB_2D_GENERIC_STATIC_POINTER_ALLOC_NULL_UNSAFE(&came_from, node);
  /* TODO do not assume that {0,0} is out */
  
  table_of_node neighbors;
  unsigned int i;
  double cost_current, cost_new;
  node current_node = *start_node;
  while(!table_of_node_contains_unsafe(goals, &current_node))
    {
      /* current = remove lowest rank item from open */
      current_node = tab_2d_astar_cost_get_node_with_min_value_unsafe(&tab_2d_costs);
      cost_current = TAB_2D_GENERIC_STATIC_POINTER_GET_ELEMENT_VALUE_UNSAFE(&tab_2d_costs, current_node.y, current_node.x, double);
      TAB_2D_GENERIC_STATIC_POINTER_SET_ELEMENT_VALUE_UNSAFE(&tab_2d_costs, current_node.y, current_node.x, DBL_MAX, double);
      
      /* add current to closed */
      tab_2d_char_set_element_value_unsafe(&is_elements_closed, current_node.y, current_node.x, 1);
      
      neighbors = get_possible_neighbors_of_node_unsafe(map, &current_node);
      for(i=0; i < neighbors.size; ++i)
	{
	  const node neighbor_node = neighbors.elements[i];
	  cost_new = cost_current + heuristic_to_multiple_goals_unsafe(&current_node, goals, heuristic_euclidean_distance_to_one_goal);
	  
	  if(TAB_2D_GENERIC_STATIC_POINTER_GET_ELEMENT_VALUE_UNSAFE(&tab_2d_costs, neighbor_node.y, neighbor_node.x, astar_cost) == 0 ||
	     cost_new < TAB_2D_GENERIC_STATIC_POINTER_GET_ELEMENT_VALUE_UNSAFE(&tab_2d_costs, neighbor_node.y, neighbor_node.x, astar_cost))
	    {
	      TAB_2D_GENERIC_STATIC_POINTER_SET_ELEMENT_VALUE_UNSAFE(&tab_2d_costs, neighbor_node.y, neighbor_node.x, cost_new, double);
	      TAB_2D_GENERIC_STATIC_POINTER_SET_ELEMENT_VALUE_UNSAFE(&came_from, neighbor_node.y, neighbor_node.x, current_node, node);
	    }
	}
      TABLE_OF_GENERIC_STATIC_POINTER_FREE_UNSAFE(&neighbors);
    }
  
  TAB_2D_GENERIC_STATIC_POINTER_FREE_UNSAFE(&tab_2d_costs);
  tab_2d_char_free_unsafe(&is_elements_closed);
  return came_from;
}

/**
 * @brief Draw a map with arrows based on predecessors.
 * @param map A not null pointer on a map/graph
 * @param predecessors Predessors
 * @param stream A not null pointer on a stream to print in
 */
static inline
void
astar_search_draw_map_without_grid_with_arrows_inverse_unsafe(const graph* map,
							      const tab_2d_node* predecessors,
							      FILE* stream)
{
  node predecessor;
  for(unsigned int line=0, column; line < map->nb_lines; ++line)
    {
      for(column=0; column < map->nb_columns; ++column)
	{
	  predecessor =
	    TAB_2D_GENERIC_STATIC_POINTER_GET_ELEMENT_VALUE_UNSAFE(predecessors,
								   line,
								   column,
								   node);
	  if(predecessor.x == 0 && predecessor.y == 0)
	    {
	      fputc(map_2d_tile_get_element_value_unsafe(map, line, column),
		    stream);
	    }
	  else if(predecessor.y == line -1 && predecessor.x == column)
	    {
	      fputs("↑", stream);
	    }
	  else if(predecessor.y == line +1 && predecessor.x == column)
	    {
	      fputs("↓", stream);
	    }
	  else if(predecessor.y == line    && predecessor.x == column -1)
	    {
	      fputs("←", stream);
	    }
	  else if(predecessor.y == line    && predecessor.x == column +1)
	    {
	      fputs("→", stream);
	    }
	  else if(predecessor.y == line -1 && predecessor.x == column -1)
	    {
	      fputs("↖", stream);
	    }
	  else if(predecessor.y == line +1 && predecessor.x == column +1)
	    {
	      fputs("↘", stream);
	    }
	  else if(predecessor.y == line -1 && predecessor.x == column +1)
	    {
	      fputs("↗", stream);
	    }
	  else if(predecessor.y == line +1 && predecessor.x == column +1)
	    {
	      fputs("↘", stream);
	    }
	  else
	    {
	      fputc('?', stream);
	    }
	}
      fputc('\n', stream);
    }
}


#endif

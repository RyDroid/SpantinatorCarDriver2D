/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "gp_map_io.h"
#include "cstring_functions.h"
#include "astar_path.h"


bool
is_a_help_option_unsafe(const char* arg)
{
  return
    cstring_equals(arg, "-?")       ||
    cstring_equals(arg, "-h")       ||
    cstring_equals(arg, "--help")   ||
    cstring_equals(arg, "--usage")  ||
    cstring_equals(arg, "--man")    ||
    cstring_equals(arg, "--manual") ||
    cstring_equals(arg, "--info");
}

bool
is_a_help_option(const char* arg)
{
  return
    arg != NULL &&
    is_a_help_option_unsafe(arg);
}

void
usage_to_stream_unsafe(FILE* stream,
		       const char* program_command)
{
  fprintf(stream,
	  "Usage: %s [options] file-path-to-a-track-txt",
	  program_command);
  fputc('\n', stream);
}

bool
remove_starts_from_map_unsafe(unsigned int argc,
			      const char* argv[])
{
  bool answer = false;
  for(unsigned int i=0; i < argc; ++i)
    {
      if(cstring_equals(argv[i], "--remove-starts") ||
	 cstring_equals(argv[i], "--delete-starts"))
	{
	  answer = true;
	}
      else if(cstring_equals(argv[i], "--keep-starts"))
	{
	  answer = false;
	}
    }
  return answer;
}


int
main(int argc, const char* argv[])
{
  if(argc == 1)
    {
      usage_to_stream_unsafe(stderr, argv[0]);
      return EXIT_FAILURE;
    }

  /* Argument management with message and exit result */
  if (is_a_help_option_unsafe(argv[1]))
    {
      usage_to_stream_unsafe(stdout, argv[0]);
      return EXIT_SUCCESS;
    }
  if(cstring_equals(argv[1], "--license") ||
     cstring_equals(argv[1], "--licence") ||
     cstring_equals(argv[1], "--copyright"))
    {
      puts("This program is under GNU Affero General Public License 3 "
	   "as published by the Free Software Foundation, "
	   "either version 3 of the License, "
	   "or (at your option) any later version.");
      return EXIT_SUCCESS;
    }
  if(cstring_equals(argv[1], "--author"))
    {
      puts("The author of this program is Nicola Spanti, "
	   "also known as RyDroid.");
      return EXIT_SUCCESS;
    }
  if(cstring_equals(argv[1], "--auteur"))
    {
      puts("Le créateur de ce programme est Nicola Spanti, "
	   "aussi connu sous le pseudonyme RyDroid.");
      return EXIT_SUCCESS;
    }
  
  gp_map map = gp_map_create_empty();
  bool remove_starts = remove_starts_from_map_unsafe(argc -2 /* program + track_path  */,
						     &argv[1]);
  
  FILE* file = fopen(argv[argc-1], "r");
  if(file == NULL)
    {
      fprintf(stderr,
	      "File %s does not exist or "
	      "this program had been invoked by a user that has not the right to read it!\n",
	      argv[argc-1]);
      return EXIT_FAILURE;
    }
  if(!gp_map_load_from_stream(&map, file, remove_starts))
    {
      fprintf(stderr, "Loading failure!\n");
      fclose(file);
      return EXIT_FAILURE;
    }
  fclose(file);
  
  
  printf("## Cars (%u)\n", map.cars.size);
  gp_map_print_cars_to_stdout_as_an_ordered_list(&map);
  putchar('\n');
  putchar('\n');
  
  puts("## Map");
  printf("nbLines=%u, nbColumns=%u\n",
	 map.map.nb_lines,
	 map.map.nb_columns);
  gp_map_print_map_to_stdout_without_grid(&map);
  
  
  puts("## Astar");
  table_of_goal_2d goals = get_table_of_goals_from_map_2d_unsafe(&map.map);
  node starter;
  for(unsigned int line=0, column; line < map.map.nb_lines; ++line)
    {
      for(column=0; column < map.map.nb_columns; ++column)
	{
	  if(map_2d_tile_get_element_value_unsafe(&map.map, line, column)
	     == MAP_TILE_START1)
	    {
	      starter.x = column;
	      starter.y = line;
	    }
	}
    }
  tab_2d_node predecessors = astar_search_unsafe(&starter, &goals, &map.map);
  astar_search_draw_map_without_grid_with_arrows_inverse_unsafe(&map.map,
								&predecessors,
								stdout);
  
  
  TAB_2D_GENERIC_STATIC_POINTER_FREE_UNSAFE(&predecessors);
  gp_map_destruct_unsafe(&map);
  
  return EXIT_SUCCESS;
}

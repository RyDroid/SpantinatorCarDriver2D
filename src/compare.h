/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief Functions to compare.
 */


#ifndef COMPARE_H
#define COMPARE_H


#include "math_macros.h"


static inline
double
min_table_of_double_unsafe(double* table, unsigned int size)
{
  double min = table[0];
  for(unsigned int i=1; i < size; ++i)
    {
      if(min > table[i])
	{
	  min = table[i];
	}
    }
  return min;
}

static inline
double
max_table_of_double_unsafe(double* table, unsigned int size)
{
  double max = table[0];
  for(unsigned int i=1; i < size; ++i)
    {
      if(max < table[i])
	{
	  max = table[i];
	}
    }
  return max;
}


#endif

/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file
 * @brief Functions to compute the cost to go to a node to an other node.
 */


#ifndef ASTAR_COST_H
#define ASTAR_COST_H


#include <math.h>
#include "compare.h"
#include "graph_2d.h"
#include "goals_2d.h"


/**
 * @brief Type for a cost in the A-start algorithm.
 */
typedef double astar_cost;


/**
 * @brief Returns the euclidean distance heuristic to one goal.
 * @param current_node The current/starting node
 * @param goal The goal
 * @return The euclidean distance heuristic to one goal
 */
static inline
astar_cost
heuristic_euclidean_distance_to_one_goal(const node current_node,
					 const node goal)
{
  /* cost for moving from one space to an adjacent space */
  static const astar_cost D = 1;
  
  unsigned int dx, dy;
  dx = abs(((signed int) current_node.x) - ((signed int) goal.x));
  dy = abs(((signed int) current_node.y) - ((signed int) goal.y));
  return D * sqrt(dx * dx + dy * dy);
}

/**
 * @brief Returns the heuristics for each goal.
 * @param current_node The current/starting node
 * @param goals The goals
 * @param heuristic_funtion A function to compute a heuristic for one goal
 * @return The heuristics for each goal
 */
static inline
astar_cost*
heuristics_of_multiple_goals_unsafe(const node* current_node,
				    const table_of_goal* goals,
				    double (*heuristic_funtion)(const node current_node,
								const node goal))
{
  astar_cost* res = (astar_cost*) malloc(goals->size * sizeof(astar_cost));
  if(res == NULL)
    {
      return NULL;
    }
  
  for(unsigned int i=0; i < goals->size; ++i)
    {
      res[i] = heuristic_funtion(*current_node, goals->elements[i]);
    }
  return res;
}

/**
 * @brief Returns the minimum heuristic to multiple goals.
 * @param current_node The current/starting node
 * @param goals The goals
 * @param heuristic_funtion A function to compute a heuristic for one goal
 * @return The minimum heuristic to multiple goals
 */
static inline
astar_cost
heuristic_to_multiple_goals_unsafe(const node* current_node,
				   const table_of_goal* goals,
				   double (*heuristic_funtion)(const node current_node,
							       const node goal))
{
  astar_cost res;
  astar_cost* heuristics = heuristics_of_multiple_goals_unsafe(current_node, goals, heuristic_funtion);
  res = min_table_of_double_unsafe(heuristics, goals->size);
  free(heuristics);
  return res;
}


#endif
